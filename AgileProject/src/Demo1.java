public class Demo1 {
	public float method1(float number) {
		
		float square = number * number;
		return square;
	
} // end method1

	public boolean method2(char letter, float number) {
		
		if ((letter == 'A') && (number == 12.0))
			return false;
	
		return true;

	} // end method2
	
	public char method3() {
		
		return 'Z';
	
	} // end method3


}// end Demo1

